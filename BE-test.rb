class Recipe
    # Store the list of recipe mapped to the ingredients
    @recipe_list = {}

    # Temporarily store the ingredient for a recipe
    @temporary_ingredient_list = []

    # Initiates a new recipe with recipe_name.
    # If there is an existing recipe with the recipe_name, the old one will get deleted.
    def self.recipe(recipe_name)
        yield if block_given?
        @recipe_list[recipe_name] = @temporary_ingredient_list.uniq
        @temporary_ingredient_list.clear
        return @recipe_list[recipe_name]
    end

    # Assign an ingredient to a recipe.
    # Should only be called from a block in recipe function.
    # If an ingredient is named twice, it will still only be counted as one.
    def self.ingredient(ingredient_name)
        @temporary_ingredient_list << ingredient_name
        return nil
    end

    # Get the latest recipe with recipe_name.
    def self.for(recipe_name)
        return Struct
            .new(:name, :ingredients)
            .new(recipe_name, @recipe_list[recipe_name])
    end
end
